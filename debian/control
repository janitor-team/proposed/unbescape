Source: unbescape
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Christopher Hoskin <mans0954@debian.org>
Build-Depends: debhelper (>= 10), default-jdk, maven-debian-helper (>= 2.1)
Build-Depends-Indep: libmaven-bundle-plugin-java (>= 2.5.4), libmaven-javadoc-plugin-java (>= 3.0.0~M1),
 default-jdk-doc
Standards-Version: 4.1.1
Vcs-Git: https://anonscm.debian.org/git/pkg-java/unbescape.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/unbescape.git
Homepage: http://www.unbescape.org

Package: libunbescape-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}, libunbescape-java-doc
Description: advanced yet easy-to-use escape/unescape library for Java
 unbescape is a Java library aimed at performing fully-featured and
 high-performance escape and unescape operations for:
 .
  * HTML5 and HTML 4
  * XML 1.0 and XML 1.1
  * JavaScript
  * JSON
  * URI / URL (both paths and query parameters)
  * CSS (both identifiers and string literals)
  * CSV (Comma-Separated Values)
  * Java literals
  * Java .properties files (both keys and values)
 .
 Its goals are:
 .
  * To be easy to use. Few lines of code needed. No additional dependencies.
  * To be fast. Faster and lighter than most other options available in Java.
  * To be versatile. Provides different escaping types and levels in order to
    better adapt to different scenarios and contexts.
  * To be feature-complete. Includes full HTML5 support, careful implementation
    of the JavaScript, JSON, Java, etc specifications, streaming support...

Package: libunbescape-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${maven:DocDepends}, ${maven:DocOptionalDepends}
Suggests: libunbescape-java
Description: escape/unescape library for Java (documentation)
 This package contains the API documentation of libunbescape-java,
 a Java library aimed at performing fully-featured and
 high-performance escape and unescape operations.
